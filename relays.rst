.. csv-table:: Relays
   :header: "FQDN", "Name", "Fingerprint", "Location"
   :widths: 20, 20, 20, 20

   "tor03.kekx.net", "makemake", "C1768D9F00A12189BAAC9B61C5B9854392271CAF", "n/a, n/a"
   "tor04.kekx.net", "cuddle", "C1734E8C2D2DC1A579794DFA209C7CBCFA2B26F7", "n/a, n/a"
   "tor05.kekx.net", "puddle", "9AF0C9EB3BDE62145DCE690CC82B9B8D79987E10", "China, Hong Kong"
   "tor06.kekx.net", "cashew", "8063D1DCE54116090DD1B03E0D602E4AABE281F1", "Germany, n/a"
   "tor-exit.zz0.network", "peanuts", "F62B74728AC72A495C986255199D9AF19CCA5B51", "USA, n/a"
   "thetrip.lysergic.dev", "thetrip", "424F9C80A25843A2E60EDACCD3092D31D300FD74", "Germany, Nuremberg"
   "cytnhia.lysergic.dev", "cynthia", "519137667154E62261381ED5B912898E7DBDC60C", "Germany, Nuremberg"
   "centauri.lysergic.dev", "centauri", "BA45EB745D44E7FEA375AFF45A4C82C51E4C2B71", "Germany, Nuremberg"
